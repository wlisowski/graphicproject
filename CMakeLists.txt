cmake_minimum_required(VERSION 3.5)

project(GraphicProject LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(SOURCE_FILES)

add_subdirectory(
    ${PROJECT_SOURCE_DIR}/libraries/gtest)
add_subdirectory(
    ${PROJECT_SOURCE_DIR}/SFML)

set(INCLUDE_DIRECTORIES 
    ${PROJECT_SOURCE_DIR}/src
    ${PROJECT_SOURCE_DIR}/src/Logger
    ${PROJECT_SOURCE_DIR}/generated_headers
    ${PROJECT_SOURCE_DIR}/SFML/include
)

add_subdirectory(application)
add_subdirectory(tests)



