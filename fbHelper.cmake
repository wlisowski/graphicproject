add_custom_target(generate ALL)

function(compile_flatbuffers_schemas SCHEMA)
  message(STATUS "Add the code-generation command for the `${SCHEMA}` schema.")
  add_custom_command(
    TARGET generate
    COMMAND cd ${CMAKE_CURRENT_SOURCE_DIR}/build/libraries/flatbuffers/ &&
    ./flatc -o ${CMAKE_CURRENT_SOURCE_DIR}/generated_headers
    --cpp ${CMAKE_CURRENT_SOURCE_DIR}/${SCHEMA}
    DEPENDS flatc)
  MESSAGE(STATUS ${COM})
endfunction()


compile_flatbuffers_schemas(data/schemas/window.fbs)

add_dependencies(generate flatc)
